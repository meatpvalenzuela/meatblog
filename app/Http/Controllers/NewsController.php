<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use DB;
use Carbon\Carbon;
use App\Events\MessageReceived;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mensajes = News::all();
       /*  $mensajes = News::all(); */
        return view('layouts.app', compact('mensajes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mensaje = News::create($request->all());

     //   event(new MessageReceived($mensaje)); 

        return redirect()->route('layouts.app');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mes = DB::table('news')->where('id', $id)->first();
        return view('news.show', compact('mes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mensaje = DB::table('news')->where('id', $id)->first();
        return view('news.edit', compact('mensaje'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('news')->where('id', $id)->update([
            "title" => $request->input('title'),
            "name" => $request->input('name'),
            "email" => $request->input('email'),
            "content" => $request->input('content'),
            "updated_at" => Carbon::now(),
        ]);

        return redirect()->route('layouts.app');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('news')->where('id', $id)->delete();
        return redirect()->back();
    }
}
