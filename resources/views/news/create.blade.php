@extends('layouts.app')

@section('content')

<div class="container">
        <form method="POST" action="{{ route('news.store') }}">
            {!! csrf_field() !!}
                <div class="form-group">
                  <label for="exampleInputEmail1">Nombre</label>
                  <input type="text" class="form-control" name="name" aria-describedby="nameHelp" placeholder="Nombre">
                  <small id="nameHelp" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Ejemplo: correo@gmail.com">
                  <small id="emailHelp" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Titulo</label>
                  <input type="text" class="form-control" name="title" aria-describedby="nameHelp" placeholder="Título">
                  <small id="nameHelp" class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                  <label for="exampleTextarea">Mensaje</label>
                  <textarea class="form-control" name="content" placeholder="Escribe tu mensaje..." rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-info">Enviar</button>
              </form>
      
</div>
@endsection
