
@extends('layouts.app')

@section('content')
    <div class="container">
      @foreach ($mensajes as $mess)
        <p> {{ $mess->content }}</p>
      @endforeach
    </div>
@endsection
