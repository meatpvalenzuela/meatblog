
require('./bootstrap');

window.Vue = require('vue');


Vue.component('guestmess', require('./components/ShowMessageForm.vue'));
Vue.component('authmess', require('./components/ShowAuthMessageForm.vue'));

const app = new Vue({
    el: '#app'
});
