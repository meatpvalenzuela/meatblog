<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>

        a { color: inherit; } 

        a:hover { color: inherit; } 

        .msg {
            color: lightblue;
            transition: all .2s ease-in-out;;
        }

        .msg:hover {
            color: lightskyblue;
            transform: scale(1.1);
        }

        textarea {
            resize: none;
        }
        
        .msgActive {
            pointer-events: none;
            cursor: default;
             text-decoration: none;
            color: grey;
            resize: none;
        }

        a.title:hover {

            color: grey !important;
        }

    </style>

</head>
<body>



    <?php function activeCreate($url) {
        return request()->is($url) ? 'msgActive' : '';
    } ?>

    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
            
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Meatblog') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>
           
                    <div class="msg">
                    <a class="{{ activeCreate('news/create') }}" href="{{ route('news.create')}}"><span><i class="fas fa-envelope fa-3x"></i></span></a>
                    </div>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
                
            </div>
        </nav>
    
        @section('content')
    
            <div class="container">
            <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h4 class="modal-title w-100 font-weight-bold">Sign in</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="md-form mb-5">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input type="email" id="defaultForm-email" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="defaultForm-email">Your email</label>
                </div>

                <div class="md-form mb-4">
                    <i class="fa fa-lock prefix grey-text"></i>
                    <input type="password" id="defaultForm-pass" class="form-control validate">
                    <label data-error="wrong" data-success="right" for="defaultForm-pass">Your password</label>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-default">Login</button>
            </div>
        </div>
    </div>

</div>
@guest
<guestmess>
</guestmess>
@else
<authmess>
</authmess>
@endguest

@if(isset($mensajes))
<div class="row">

<div class="col-sm">
<div class="card-columns" style="display: inline-block !important;">
@foreach ($mensajes as $mes)
<div class="card" style="display: inline-block !important; max-width: 300px; ">
    <img class="card-img-top" src="http://via.placeholder.com/400x300" alt="Card image cap">
    <div class="card-body">
      <a class="card-link title" style="color: #00A3D9;" href="{{ route('news.show', $mes->id) }}"><h5 class="card-title">{{ $mes->title }}</h5></a>
      <p class="card-text">{{ $mes->content }}</p>
   
    
     
      <small class="text-left text-muted">
      
      {{ $mes->name }}
      
      </small> |
      <small class="text-right text-muted">
      {{ $mes->email }}
      </small>
      @guest
      
      @else
      <form style="display:inline;" method="POST" action="{{ route('news.destroy', $mes->id) }}">
      {!! csrf_field() !!}
      {!! method_field('DELETE') !!}
      <button type="submit" class="btn pull-right btn-danger btn-sm" style="float: right; margin-left: 5px;"><span><i class="fas fa-trash-alt"></i></span></button></a>
      </form>
      <a href="{{ route('news.edit', $mes->id) }}"><button class="btn pull-right btn-secondary btn-sm" style="float: right;"><span><i class="fas fa-pencil-alt"></i></span></button></a>
      @endguest
      </div>
</div>
@endforeach
</div>
@endif
</div>
</div>
<!-- <div class="text-center">
    <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Launch Modal Login Form</a>
</div> -->



      <!--       <table class="table">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Mensaje</th>
                    <th>Acciones</th>
                </tr>
                </thead>
                <tbody>

                @if (isset($mensajes))
                     @foreach ($mensajes as $mess)
                        <tr>
                            <td>{{ $mess->title }}</td>
                            <td>
                                <a href="{{ route('news.show', $mess->id) }}">
                                    {{ $mess->name }}
                                </a>
                            </td>
                            <td>{{ $mess->email }}</td>
                            <td>{{ $mess->content }}</td>
                            <td>
                            <a href="{{ route('news.edit', $mess->id) }}"><button class="btn btn-secondary btn-sm">update</button></a>
                            <form style="display:inline;" method="POST" action="{{ route('news.destroy', $mess->id) }}">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                                 <button type="submt" class="btn btn-danger btn-sm">delete</button></a>
                            </form>
                            </td>
                        </tr>
                     @endforeach
                     </tbody>
                     </table>
                @else 
                <div>
                 <h1>no hay mensajes</h1>
                </div>
                @endif
            </div> -->
        @endsection

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
