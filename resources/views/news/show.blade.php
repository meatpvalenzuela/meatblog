
@extends('layouts.app')

@section('content')

<div class="card">
    <!-- <img class="card-img-top" src="http://via.placeholder.com/239x180" alt="Card image cap"> -->
    <div class="card-body">
    <h5 class="card-title" style="color: #00A3D9;">{{ $mes->title }}</h5>
      <p class="card-text">{{ $mes->content }}</p>
    </div>
    <div class="card-footer">
      <small class="text-left text-muted">
      <a href="{{ route('news.show', $mes->id) }}">
      {{ $mes->name }}
      </a>
      </small> |
      <small class="text-right text-muted">
      {{ $mes->email }}
      </small>
      @guest
      @else
      <form style="display:inline;" method="POST" action="{{ route('news.destroy', $mes->id) }}">
      {!! csrf_field() !!}
      {!! method_field('DELETE') !!}
      <button type="submit" class="btn pull-right btn-danger btn-sm" style="float: right; margin-left: 5px;"><span><i class="fas fa-trash-alt"></i></span></button></a>
      </form>
      <a class="card-link" href="{{ route('news.edit', $mes->id) }}"><button class="btn pull-right btn-secondary btn-sm" style="float: right;"><span><i class="fas fa-pencil-alt"></i></span></button></a>
      @endguest
    </div>
</div>
@endsection
