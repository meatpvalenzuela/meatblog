<?php


Route::get('/', function () {
    return view('layouts/app');
});


Route::get('users/{id}/show', ['as' => 'users.show', 'uses' => 'UsersController@show']);

Route::get('/', ['as' => 'layouts.app', 'uses' => 'NewsController@index']);
Route::get('news/{id}/show', ['as' => 'news.show', 'uses' => 'NewsController@show']);
Route::get('news/create', ['as' => 'news.create', 'uses' => 'NewsController@create']);
Route::post('/', ['as' => 'news.store', 'uses' => 'NewsController@store']);
Route::get('news/{id}/edit', ['as' => 'news.edit', 'uses' => 'NewsController@edit']);
Route::put('news/{id}', ['as' => 'news.update', 'uses' => 'NewsController@update']);
Route::delete('news/{id}', ['as' => 'news.destroy', 'uses' => 'NewsController@destroy']);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
